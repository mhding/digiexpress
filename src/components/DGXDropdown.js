import { memo, useEffect, useRef, useState } from "react";

const DGXDropdown =  ({ list, getValue, defaultt, init,moreElement }) => {

  const [showList,setShowList] = useState(false);
  const [selected,setSelected] = useState(null)

  const refDrop = useRef(null);

  const handleClick = () => setShowList(true);

  const selectItem = (item) => {
    getValue(item);
    setSelected(item)
    setShowList(false);
  };

  useEffect(() => {
    if(defaultt){
      const item = list?.find(item => item.key == defaultt)
      if(!!item){
        setSelected(item)
        getValue (item)
      }else{
        setSelected(init)
      }
    }
  },[defaultt,list]);

  const handleOutsideClick = (event) => {
    refDrop.current && !refDrop.current.contains(event.target) && setShowList(false);
  };

  useEffect(() => {
    document.addEventListener("click", handleOutsideClick);
    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  }, []);


  return (
    <>
      <div className="drop-down"  ref={refDrop}>
        <div
          className={`input size text color ${showList && "active"}`}
          onClick={handleClick}
        >
          {selected?.name}
        </div>
        {showList && (
          <div 
          className="list size color">
            {list?.map(item => {
              return(
                <div
                key={item?.key}
                className="item size text"
                onClick={() => selectItem(item)}>
                {moreElement && moreElement(item?.key)}
                {item?.name}
              </div>
              )
            })}
          </div>
        )}
      </div>
    </>
  );
};


export default memo(DGXDropdown);