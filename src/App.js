import { useCallback, useEffect, useState } from "react";
import DGXLogo from "./components/DGXLogo";
import DGXDropdown from "./components/DGXDropdown";
import DGXContainer from "./components/DGXContainer";
import data from './mock/drop-items.json'
import defaultt from './mock/form-data.json'
import "./App.css";
import "./fonts/font.css";

function App() {

  const init = Object.create({
    key: -1,
    name: "یک مورد را انتخاب کنید",
  })

  const [list,setList] = useState([]);
  const [selectedVal,setSelectedVal] = useState(init)

  useEffect(() => setList(data) ,[data])

  const returnResult = useCallback(val =>setSelectedVal(val),[]) ;

  return (
    <div
      id="app"
      className="flex"
      style={{ backgroundImage: 'url("/assets/pixel-arts/pixel-wall.png")' }}
    >
      <DGXContainer className="top">
        <DGXLogo className="flex" />
        <div className="mt">
          <DGXDropdown
          getValue={value => returnResult(value)}
          list={list}
          init={init}
          defaultt={defaultt?.dropdown}
          moreElement={index =>
            <span>
              <img className="img-icon" src={list[index - 1]?.img} />
              {list[index - 1]?.brand} / 
            </span>
          }
          />
        </div>
        <div className="result">{selectedVal?.key == -1 ? 'هیچ!': selectedVal?.name}</div>
      </DGXContainer>
    </div>
  );
}

export default App;
